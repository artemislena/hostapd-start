1. Clone the repository

git clone https://gitlab.com/FantasyCookie17/hostapd-start

2. Change to directory

cd ./hostapd-start/

3. Prepare the service

For systemd:
sudo cp hostapd-start.service /lib/systemd/system
sudo cp start.sh /etc/hostapd/

For Runit:
sudo mkdir /etc/sv/hostapd-start
sudo cp start.sh /etc/sv/hostapd-start/run

4. Enable and start service (note: disable and stop the default hostapd service before you do this)

For systemd:
sudo systemctl enable hostapd-start && sudo systemctl start hostapd-start

For Runit:
sudo ln -s /etc/sv/hostapd-start /var/service/